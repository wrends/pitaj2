<?php

class AnswersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /answers
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /answers/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /answers
	 *
	 * @return Response
	 */
	public function store( $id )
	{
		if ($question = Question::findOrFail($id)) {
			if($question->owner->id === Auth::user()->id)
			{
				Flash::error('Odgovaranje na vlastito pitanje nije dozvoljeno');
				return Redirect::back();
			}
			if($question->answers()->where('user_id', Auth::user()->id)->first())
			{
				Flash::error('Navedeno pitanje već sadrži Vaš odgovor');
				return Redirect::back();
			}

			$answer = Answer::create(Input::all());
			$answer->owner()->associate(Auth::user());
			$answer->question()->associate($question);
			$answer->save();

			Flash::success('Odgovor je poslan');
			return Redirect::back();
		}

		Flash::error('Greška');
		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 * GET /answers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /answers/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /answers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /answers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}