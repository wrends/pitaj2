<?php
use QA\FormValidation\QuestionForm;
use Laracasts\Validation\FormValidationException;

class QuestionsController extends \BaseController {

	protected $replaceChars = [
		'đ'	=> 'd',
		'č'	=> 'c',
		'ć'	=> 'c',
		'š'	=> 's',
		'ž'	=> 'z'
	];
	protected $questionForm;
	public function __construct(QuestionForm $questionForm)
	{

		$this->questionForm = $questionForm;
	}

	/**
	 * Display a listing of the resource.
	 * GET /question
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /question/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('questions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /question
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		// dd((Auth::check()) ? 'true' : 'false');
		try {
			$tagsAssoc = [];
			$tagIds = [];

			if($tags = Input::get('tags'))
			{
				// mb_strtolower('Ć', 'UTF-8');
				$tags = explode(',', trim($tags));
				$maxCount = min(3, count($tags));
				$tags = array_slice($tags, 0, $maxCount);


				foreach ($tags as $tag) {
					$key = strtr( mb_strtolower($tag, 'UTF-8'), $this->replaceChars);

					// $key = implode('-', explode(' ', $key));

					$value = mb_strtolower($tag, 'UTF-8');
					$tagsAssoc[$key] = $value;
				}
			}
			$this->questionForm->validate($input);

			$question = new Question;
			$question->title = rtrim(Input::get('title'), '?') . '?';
			$question->description = Input::get('description');
			$question->owner()->associate(Auth::user());
			foreach ($tagsAssoc as $key => $value)
			{
				if (Tag::whereSlug($key)->get()->count())
				{
					$tag = $tag = Tag::whereSlug($key)->first();
					$tag->increment('count');
				} else {
					$tag = new Tag;
					$tag->slug = $key;
					$tag->name = $value;
					$tag->save();
				}
				array_push($tagIds, $tag->id);
			}

			$question->save();
			$question->tags()->sync($tagIds);



		} catch (FormValidationException $e) {
			return Redirect::back()->withInput()
			                       ->withErrors($e->getErrors());
		}
		Flash::success('Tvoj pitanje je poslano');
		return Redirect::to('/');
	}

	/**
	 * Display the specified resource.
	 * GET /question/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$question = Question::with('answers.owner')->findOrFail($id);

		Event::fire('question.viewed', [$question]);

		return View::make('questions.show', compact('question'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /question/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /question/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /question/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}