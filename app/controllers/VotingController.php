<?php

use QA\Services\VotingService;

class VotingController extends \BaseController {

	protected $votingService;
	protected $segment;

	public function __construct()
	{
		// $this->beforeFilter('ajax');

		if ($this->routeSegmentValid())
			$this->votingService = App::make('QA\Services\VotingService', [$this->segment]);
	}


	public function voteUp($id)
	{
		if ($this->votingService->voteUp($id))
			return Response::json(array('status' => 'OK'));
		else
			return Response::json(array('status' => 'ERROR'));
	}


	public function voteDown($id)
	{
		if ($this->votingService->voteDown($id))
			return Response::json(array('status' => 'OK'));
		else
			return Response::json(array('status' => 'ERROR'));
	}

	protected function routeSegmentValid()
	{
		$arg = rtrim(Request::segment(2), 's');
		if (in_array($arg, ['question', 'answer']))
		{
			$this->segment = $arg;
			return true;
		}
		return false;
	}
}