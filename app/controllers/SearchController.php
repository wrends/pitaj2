<?php

class SearchController extends \BaseController {

	/**
	 * POST /search
	 *
	 * @return Response
	 */
	public function search()
	{
		if ($searchQuery = trim(Input::get("q")))
		{
			$questions = Question::search($searchQuery)->paginate(10);

			return View::make('search', compact('questions'))
						->with([
							'searchQuery' => $searchQuery,
							'count'		  => $questions->count()
						]);
		}
	}



}