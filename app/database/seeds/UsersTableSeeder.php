<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 34) as $index)
		{
			User::create([
				'username'		=> $faker->name,
				'email'			=> $faker->email,
				'password'		=> 'pass'
			]);
		}
	}

}