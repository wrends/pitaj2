<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		DB::table('users')->delete();
		DB::table('questions')->delete();
		DB::table('answers')->delete();
		DB::table('comments')->delete();
		DB::table('tags')->delete();
		DB::table('question_tag')->delete();


		$this->call('UsersTableSeeder');
		$this->call('TagsTableSeeder');
		$this->call('QuestionsTableSeeder');
		$this->call('AnswersTableSeeder');
		$this->call('CommentsTableSeeder');

	}

}
