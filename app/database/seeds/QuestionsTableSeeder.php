<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class QuestionsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$usersPool = User::lists('id');
		$tagsPool = Tag::lists('id');

		foreach(range(1, 90) as $index)
		{

			$question = new Question;
			$question->user_id = User::find(array_rand($usersPool) + 1)->id;
			$question->title = rtrim($faker->sentence, '.') . '?';
			$question->description = $faker->text;
			$question->views = array_rand(range(0, 28));
			$question->save();

			$question = Question::find($index);
			$question->tags()->attach([Tag::find(array_rand($tagsPool) + 1)->id]);
			$question->tags()->attach([Tag::find(array_rand($tagsPool) + 1)->id]);
			$question->tags()->attach([Tag::find(array_rand($tagsPool) + 1)->id]);

		}
	}

}