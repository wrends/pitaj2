<?php

class Answer extends \Eloquent {
	protected $fillable = ['answer'];

	public function owner()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function question()
	{
		return $this->belongsTo('Question');
	}

	public function scopeNewestFirst($q)
	{
		return $q->orderBy('created_at', 'desc');
	}

	public function scopeBestFirst($q)
	{
		return $q->orderBy('vote_count', 'desc');
	}



}