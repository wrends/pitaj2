<?php

class Tag extends \Eloquent {
	protected $fillable = ['name'];

	public function questions()
	{
		return $this->belongsToMany('Question')->withTimestamps();
	}
}