<?php namespace QA\Services;

class VotingService {

	protected $modelName;

	public function __construct($modelName)
	{
		$this->modelName = $modelName;
	}

	public function voteUp($id)
	{
		if ( ! $this->ownerVote($id) and ! $this->modelVoteExists($id))
		{

				\Auth::user()->{$this->relation()}()->attach([$id => ['type' => 1]]);
				\Auth::user()->{$this->relation()}->find($id)->increment('vote_count');
				return true;

		}
			return false;

	}

	public function voteDown($id)
	{
		if ( ! $this->ownerVote($id) and ! $this->modelVoteExists($id))
		{

				\Auth::user()->{$this->relation()}()->attach([$id => ['type' => -1]]);
				\Auth::user()->{$this->relation()}->find($id)->decrement('vote_count');
				return true;
		}
			return false;
	}

	public function modelVoteExists($model_id)
	{

		$model = strtolower($this->modelName);
		return \Auth::user()->{$this->relation()}->contains($model_id);
		// return \User::find(2)->{$this->relation()}->contains($model_id);
	}


	public function relation()
	{
		$model = strtolower($this->modelName);
		return "{$model}Votes";
	}

	public function isVotedDown($id)
	{
		return (bool) \Auth::user()->{$this->relation()}->find($id)->pivot->type < 0 ;
		// return (int) \User::find(2)->{$this->relation()}->find($id)->pivot->type < 0 ;
	}

	public function ownerVote($id)
	{
		$model = \App::make("\\" . ucfirst($this->modelName));
		return ($model->find($id)->user_id === \Auth::user()->id) ? true : false;
	}


}