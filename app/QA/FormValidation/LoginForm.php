<?php namespace QA\FormValidation;

use Laracasts\Validation\FormValidator;

class LoginForm extends FormValidator {

	protected $rules = [
		'email' => 'required',
		'password' => 'required'
	];

}