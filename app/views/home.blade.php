@extends('layout.default')

@section('content')

<div class="row">
<div class="col-md-8">
@include('layout.fragments.home-search')
<a href="/questions/create" class="btn btn-success my-btn-success btn-lg pull-right">+ &nbsp; Postavi pitanje</a>
<div class="tabbable">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#pane1" data-toggle="tab">Neodgovorena</a></li>
    <li><a href="#pane2" data-toggle="tab">Popularna</a></li>
    <li><a href="#pane3" data-toggle="tab">Lorem ipsum</a></li>
  </ul>
  <div class="tab-content">
    <div id="pane1" class="tab-pane active">





    @foreach($questionsUnanswered as $question)
    	@include('layout.fragments.question-listing')
    @endforeach





    </div>
    <div id="pane2" class="tab-pane">
    @foreach($questionsPopular as $question)
    	@include('layout.fragments.question-listing')
    @endforeach
    </div>
    <div id="pane3" class="tab-pane">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>

  </div><!-- /.tab-content -->
</div><!-- /.tabbable -->


</div>


<div class="col-md-3">
  @include('layout.fragments.sidebar')
</div>

</div><!--row-->
@stop