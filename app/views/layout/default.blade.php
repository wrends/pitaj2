<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!--[if lt IE 9]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<![endif]-->
	<title>Pitaj! - Demo projekt</title>
	<meta name="description" content="">
	<meta name="author" content="webThemez">
		<!--[if lt IE 9]>
				<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
 {{ HTML::style('css/bootstrap.min.css') }}
 {{ HTML::style('//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') }}
 {{ HTML::style('css/tokenfield-typeahead.min.css') }}
 {{ HTML::style('css/bootstrap-tokenfield.css') }}
 {{ HTML::style('css/isotope.css') }}
 {{ HTML::style('css/styles.css') }}
 {{ HTML::style('font/css/font-awesome.min.css') }}
 {{ HTML::style('js/fancybox/jquery.fancybox.css') }}
 {{ HTML::style('css/da-slider.css') }}
 {{ HTML::style('//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc2/css/bootstrap-glyphicons.css') }}
 {{ HTML::style('css/answers.css') }}
 {{ HTML::style('css/wbbtheme.css') }}
 {{ HTML::style('css/my-style.css') }}
<!-- <link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/isotope.css" media="screen" />
<link rel="stylesheet" href="js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/da-slider.css" />
<link rel="stylesheet" href="css/styles.css"/>-->
<!-- Font Awesome -->
<style type="text/css" media="screen">

</style>
</head>

<body>

@include('layout.fragments.header')
@include('layout.fragments.jumbo')
<div class="container">
<div class="row">

	@yield('content')


</div><!-- row -->
</div><!--content-->

<!--/#north-america-->

<footer class="page-section">
	<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-6 about">
						<a href="#" class="logoDark"><h4>Pitaj! - Demo</h4></a>
						<p>Lorem ipsum dolor amet, consectetur adipiscing elit. Aenean
						leo lectus sollicitudin convallis eget libero. Aliquam laoreet
						tellus ut libero semper, egestas velit malesuada. Sed non
						risus eget dolor amet vestibulum ullamcorper. Integer feugiat
						molestie.</p>
						<ul class="socialIcons">
							<li><a href="#" class="fbIcon" target="_blank"><i class="fa fa-facebook-square fa-lg""></i></a></li>
							<li><a href="#" class="twitterIcon" target="_blank"><i class="fa fa-twitter-square fa-lg""></i></a></li>
							<li><a href="#" class="googleIcon" target="_blank"><i class="fa fa-google-plus-square fa-lg""></i></a></li>
							<li><a href="#" class="pinterest" target="_blank"><i class="fa fa-pinterest-square fa-lg""></i></a></li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-6 twitter">
						<h4>Latest Tweets</h4>
						<ul>
							<li><a href="#">@John Doe</a> Lorem ipsum dolor amet, consectetur adipiscing
							elit. Aenean leo lectus sollicitudin eget libero.<br><span>2 minutes ago</span></li>
							<li><a href="#">@John Doe</a> Lorem ipsum dolor amet, consectetur adipiscing
							elit. Aenean leo lectus sollicitudin eget libero.<br><span>About an hour ago</span></li>
						</ul>
					</div>
					<div class="col-md-4 contact">
						<h4>Kontakt</h4>
						<p>Lorem ipsum dolor amet, consectetur adipiscing ipsum dolor.</p>
						<ul>
							<li><i class="fa fa-phone"></i>091/553-4844</li>
							<li><a href="#"><i class="fa fa-envelope-o"></i> wrends@gmail.com</a></li>
							<li><i class="fa fa-flag"></i></li>
						</ul>
					</div>
				</div><!-- END Row -->
			</div>
</footer>
<!--/.page-section-->
<section class="copyright">
		 <div class="container">
			<div class="row">
				<div class="col-sm-12">
						Copyright {{date('Y') }} Sva prava pridržana         <div class="pull-right">Template by <a href="http://webThemez.com"> WebThemez.com</a></div>
				</div>
			</div>  <!-- / .row -->
		</div>
</section>
		<a href="#top" class="topHome"><i class="fa fa-chevron-up fa-2x"></i></a>

<!--[if lte IE 8]><script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script><![endif]-->
 {{ HTML::script('js/modernizr-latest.js') }}
 {{ HTML::script('js/jquery-1.8.2.min.js') }}
 {{ HTML::script('//code.jquery.com/ui/1.10.3/jquery-ui.js') }}
 {{ HTML::script('js/bootstrap-tokenfield.js') }}
 {{ HTML::script('js/bootstrap.min.js') }}
 {{ HTML::script('js/jquery.isotope.min.js') }}
 {{ HTML::script('js/fancybox/jquery.fancybox.pack.js') }}
 {{ HTML::script('js/jquery.nav.js') }}
 {{ HTML::script('js/jquery.cslider.js') }}
 {{ HTML::script('js/custom.js') }}
 {{ HTML::script('js/jquery.wysibb.min.js') }}

<script>
$(document).ready(function() {
	var wbbOpt = {
		buttons: "bold,italic,underline,|,img,link,video,|,bullist,numlist,|,code,quote"
	};

	$(function() {
	  $("#editor").wysibb(wbbOpt);
	});


	$('#tokenfield').tokenfield({
	  autocomplete: {
	    // source: ['red','blue','green','yellow','violet','brown','purple','black','white'],
	    delay: 100
	  },
	  showAutocompleteOnFocus: true
	});
});


$(document).ready(function () {
	$(".comments-list").addClass('hide');

	$(".toggle-comments").click(function(){
		$(".comments-list").toggleClass('hide');
		return false;
	});
});


$(document).ready(function() {

	$('.vote-up').click(function() {
		var $this = $(this);
		var countElemBinded =  '#' + $this.data("bind");
		var route = $this.attr('href');

		$.get(route, function(response) {
			if (response.status === "OK") {
				var newValue = parseInt($(countElemBinded).text(), 10) + 1;
				$(countElemBinded).text(newValue);
				console.log(newValue);
			}
		});
		return false; // prevent default
	});


	$('.vote-down').click(function() {
		var $this = $(this);
		var countElemBinded =  '#' + $this.data("bind");
		var route = $this.attr('href');

		$.get(route, function(response) {
			if (response.status === "OK") {
				var newValue = parseInt($(countElemBinded).text(), 10) - 1;
				$(countElemBinded).text(newValue);
				console.log(newValue);
			}
		});
		return false; // prevent default
	});
});

</script>
</body>
</html>