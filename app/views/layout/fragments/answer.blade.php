<div class="row">
	<div class="col-sm-2">


<div class="voteBox pull-right" style="width:50px;font-size:21pt;text-align:center;">
	<div>
		<a class="vote-up" data-bind="answer-vote-count-{{$answer->id}}" href="/vote/answers/{{$answer->id}}/up" title="">
		<span class="fa fa-chevron-up fa-2x" style="width:50px;font-size:21pt;text-align:center;" title="+1 Vote Up (This is useful)"></span>
		</a>
	</div>
	<div style="color:#737373;" id="answer-vote-count-{{$answer->id}}" data-id="answer" title="Number of user votes (likes)">
	{{$answer->vote_count}}
	</div>
	<div>
		<a class="vote-down" data-bind="answer-vote-count-{{$answer->id}}" href="/vote/answers/{{$answer->id}}/down" title="">
		<span class="fa fa-chevron-down fa-2x" style="width:50px;font-size:21pt;text-align:center;" title="-1 Vote Down (This is useful)"></span>
		</a>
	</div>
</div>



	</div>
	<div class="col-sm-8">
		<div class="panel panel-white post panel-shadow answer">
			<div class="post-heading">
				<div class="pull-left image">
					<img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
				</div>

				<div class="pull-left meta">
					<div class="title h5">
						<a href="#"><b>{{ $answer->owner->username }}</b></a>
						je dao odgovor
					</div>
					<h6 class="text-muted time">{{$answer->created_at->diffForHumans()}}</h6>
				</div>
			</div>
			<div class="post-description">
				<p>{{ BBCode::parse($answer->answer) }}</p>
				<div class="stats">
					<a href="#" class="btn btn-default stat-item">
						<i class="fa fa-thumbs-up icon"></i>2
					</a>
					<a href="#" class="btn btn-default stat-item">
						<i class="fa fa-share icon"></i>12
					</a>
					<a href="" class="btn btn-default stat-item toggle-comments">
						<i class="fa fa-share icon"></i>Show comments
					</a>
				</div>
			</div>
			<div class="post-footer">
				<div class="input-group">
					<input class="form-control" placeholder="Add a comment" type="text">
					<span class="input-group-addon">
						<a href="#"><i class="fa fa-edit"></i></a>
					</span>
				</div>
				<ul class="comments-list">
					<li class="comment">
						<a class="pull-left" href="#">
							<img class="avatar" src="http://bootdey.com/img/Content/user_1.jpg" alt="avatar">
						</a>
						<div class="comment-body">
							<div class="comment-heading">
								<h4 class="user">Gavino Free</h4>
								<h5 class="time">5 minutes ago</h5>
							</div>
							<p>Sure, oooooooooooooooohhhhhhhhhhhhhhhh</p>
						</div>
						<ul class="comments-list">
							<li class="comment">
								<a class="pull-left" href="#">
									<img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar">
								</a>
								<div class="comment-body">
									<div class="comment-heading">
										<h4 class="user">Ryan Haywood</h4>
										<h5 class="time">3 minutes ago</h5>
									</div>
									<p>Relax my friend</p>
								</div>
							</li>
							<li class="comment">
								<a class="pull-left" href="#">
									<img class="avatar" src="http://bootdey.com/img/Content/user_2.jpg" alt="avatar">
								</a>
								<div class="comment-body">
									<div class="comment-heading">
										<h4 class="user">Gavino Free</h4>
										<h5 class="time">3 minutes ago</h5>
									</div>
									<p>Ok, cool.</p>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
</div>
</div>