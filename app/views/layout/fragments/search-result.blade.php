<div class="hr-line-dashed"></div>
<div class="search-result">
	<h4>{{ link_to_route('questions.show', $question->title, $params = [$question->id]) }}</h4>
	<p>{{ Str::limit($question->description, 110) }}</p>
</div>