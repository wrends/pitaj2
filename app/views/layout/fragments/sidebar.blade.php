<aside>
<div class="well">
<h3><span class="label label-default">{{ $stats['questionTotal'] }} pitanja</span></h3>
<h3><span class="label label-default">{{ $stats['answerTotal'] }} odgovora</span></h3>
<h3><span class="label label-default">{{ $stats['userTotal'] }} korisnika</span></h3>
</div>

    <div class="list-group">

        <a href="#" class="list-group-item active">

</span><i class="fa fa-users"></i>
 Top korisnici

        </a>

        @foreach($topUsers as $topUser)
        <a href="#" class="list-group-item">

            <i class="fa fa-trophy"></i></span> {{ Str::limit($topUser->username, 15)}} <span class="badge">{{$topUser->answer_count}}</span>

        </a>
        @endforeach()

    </div>
	<div class="hr-line-dashed"></div>

	<div style="padding-left: 5px;" class"">
	<h4><i class="fa fa-star-o"></i>Top tagovi</h4>
	<div class="hr-line-dashed"></div>
	@foreach($tags as $tag)
		@include('layout.fragments.tag')
	@endforeach
</div>













</div>
</aside>