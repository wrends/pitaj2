<header class="header">
	<div class="container">
		<nav class="navbar navbar-inverse" role="navigation">
			<div class="navbar-header">
				<button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="/" class="navbar-brand scroll-top logo "><img src="/images/question_logo.png" width="55px" height="55px" alt="Pitaj!" style="margin-top:-16px;" ><b>Pitaj!</b></a>
			</div>
			<!--/.navbar-header-->
			<div id="main-nav" class="collapse navbar-collapse">
				<ul class="nav navbar-nav" id="mainNav">
					<li class="active"><a href="/" class="scroll-link"><i class="fa fa-home color"></i>&nbsp;Home</a></li>
					<li><a href="#" class="scroll-link">Pitanja</a></li>
					<li><a href="#" class="scroll-link">Kategorije</a></li>
					<li><a href="/register" class="scroll-link">Registracija</a></li>

					@if(Auth::check())
						<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-user"></i>
							<i class="fa fa-chevron-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="#">Moj profil</a></li>
							<li class="divider"></li>
							<li><a href="/logout">Logout</a></li>
						 </ul>
					  </li>
				  @else
					<li><a href="/register" class="scroll-link"><i class="fa fa-sign-in"></i>&nbsp;Login</a></li>
				  @endif

				</ul>
			</div>
			<!--/.navbar-collapse-->
		</nav>
		<!--/.navbar-->
	</div>
	<!--/.container-->
</header>
<!--/.header-->