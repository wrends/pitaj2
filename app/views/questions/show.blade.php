@extends('layout.default')
@section('content')

<div class="container">
<div class="row">
						<br>
						<div class="col-md-2 col-sm-3 text-center">


<table>







<tbody><tr>
                                    <td colspan="2" title="The Bootstrap `.icon` class will be replaced with `.glyphicon`.">
                                    <a class="fav pull-right" href="#" title="Favorite this"><i class="icon-star-empty icon-large"></i>&nbsp;</a> <span class="ellip"></span>
                                    </td>
                                </tr>
                                <tr>
                                     <td>
                                        <span class="badge my-badge" title="Impressions (views) of this ply">{{ $question->views }} &middot; <i class="glyphicon glyphicon-eye-open"></i></span><br><br>

                                        <a href="/like/bootstrap-3+glyphicons+icons"><small></small></a><br>


                                    </td>
                                    <td style="width:80px;">
                                        <div class="voteBox" style="width:50px;font-size:21pt;text-align:center;">
                                            <div><a class="vote-up" data-bind="question-vote-count-{{$question->id}}" href="/vote/questions/{{$question->id}}/up" title=""><span class="fa fa-chevron-up fa-2x" style="width:50px;font-size:21pt;text-align:center;" title="+1 Vote Up (This is useful)"></span></a></div>
                                            <div style="color:#737373;" id="question-vote-count-{{$question->id}}" title="Number of user votes (likes)">{{$question->vote_count}}</div>
                                            <div><a class="vote-down" data-bind="question-vote-count-{{$question->id}}" href="/vote/questions/{{$question->id}}/down" title=""><span class="fa fa-chevron-down fa-2x" style="width:50px;font-size:21pt;text-align:center;" title="-1 Vote Down (This is useful)"></span></a></div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="2">



                                        <a href="/tagged/bootstrap-3" title="bootstrap bootstrap-3"><span class="label">bootstrap-3</span></a>

                                        <a href="/tagged/glyphicons" title="bootstrap glyphicons"><span class="label">glyphicons</span></a>

                                        <a href="/tagged/icons" title="bootstrap icons"><span class="label">icons</span></a>


                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <em><small>Pitao/la:</small></em> <a href="" title="The Bootstrap `.icon` class will be replaced with `.glyphicon`."><small>{{$question->owner->username}}</small></a>
                                    </td>
                                </tr>

														</tbody></table>
						</div>
						<div class="col-md-10 col-sm-9">
							<h3>{{ $question->title }}</h3>
							<div class="row">
								<div class="col-xs-9">
								<p>{{ BBCode::parse($question->description) }}</p>
									@if($question->tags)
										<p class="pull-right">
										@foreach($question->tags as $tag)
											@include('layout.fragments.tag')
										@endforeach
										</p>
									@endif

									<ul class="list-inline badge"><li><a href="#">{{ $question->created_at->diffForHumans() }}</a></li><li><a href="#"><i class="glyphicon glyphicon-comment"></i> {{ $question->answers->count() }} Odgovora</a></li><li><a href="#"><i class="glyphicon glyphicon-share"></i> 34 Shares</a></li></ul>
									</div>
								<div class="col-xs-3"></div>
							</div>
							<br><br>
						</div>

</div><!-- row -->
<div class="row">
    <div class="hr-line-dashed"></div>
        @if(count($question->answers))
        <div class="container">
            @foreach($question->answers as $answer)
                @include('layout.fragments.answer')
            @endforeach
            @include('layout.fragments.answer-form')
        </div><!-- container -->
        @else
        <h4>Ovo pitanje nažalost još nema odgovor</h4>
        @endif
    </div>
</div>
@stop